<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\Category;
use App\Entity\Tags;
use App\Form\PostType;
use App\Form\CategoryType;
use App\Form\TagsType;
use App\Repository\PostRepository;
use App\Repository\CategoryRepository;
use App\Repository\TagsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutPageController extends AbstractController
{
    /**
     * @Route("/about", name="about_page")
     */
    public function index(PostRepository $postRepository, CategoryRepository $categoryRepository, TagsRepository $tagsRepository): Response
    {
        return $this->render('about_page/index.html.twig', [
            'posts' => $postRepository->findAll(),
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagsRepository->findAll()
            ]);
    }
}
